This is a repository, containing a console application exercise, made by me, and solution in different languages.

The task should serve as a "final exam" for developers who are learning the basics of a programming language (where possible).

# ctfc

`ctfc` is the "**c**onsole **t**ext **f**ile **c**hat", and the project you're going to work on. As the name suggests, it will be a chatting application, entirely in the console. For data storage, simple text files will be used.

## General structure

### Feature overview

- User registration, login and updating
- Chat rooms with title and details, message history, real time chat updating
- Direct user messaging with message, real time updating

### User accounts

Each user must have the following properties:
- `string UserName`: between 2 and 40 characters (A-z, 0-9, ., \*, \_, -, at least one letter, not already existing)
- `string Password`: between 5 and 40 characters (any character, but at least one number)
- `string Name`: between 2 and 60 characters (A-z, spaces, ', -)

Two users can't have the same UserName. Once set, a UserName cannot be changed.

### Chat rooms

Each chat room must have the following properties:
- `string Name`: between 2 and 20 characters (A-z, 0-9, spaces, \_, -, at least one letter, not already existing)
- `string Description`: between 0 and 30 characters (any character)
- `string Owner`: between 2 and 40 characters (must be an existing UserName, it's set automatically)
- `string collection Messages`: a collection containing all messages

Two chat rooms can't have the same Name. Once set, a name cannot be changed.
Once set, an owner cannot be changed (owner is set via code, it's not chosen by user).

### Direct messages chat

Each direct message chat has the following properties:
- `User Author1`: an object (or potentially a string that is just a username) for the first user in the direct messages
- `User Author2`: an object (or potentially a string that is just a username) for the second user in the direct messages
- `string collection Messages`: a collection containing all messages

The name is not chosen by a user. It is in the format `USERNAME USERNAME`, where the "bigger" username is first, and the smaller is second.

### Files

As discussed, files are used for long term storage. The location of all files are up to you, but they should follow this structure:
```
├─ Users
│  ├─ john.txt
│  ├─ robertson94.txt
│  └─ steve.txt
├─ Chat Rooms
│  ├─ Cool Kids Club.txt
│  └─ Computer geeks.txt
└─ Direct Messages
   └─ robertson94 steve.txt
```
- Users folder: contains text files for each user, in format `USERNAME.txt`
  - Each file is in the format:
    ```
    USERNAME
    PASSWORD_HASH
    NAME
    ```
- Chat Rooms folder: contains text files for each chat room, in format `CHAT ROOM NAME.txt`
  - Each file is in the format:
    ```
    NAME
    DESCRIPTION
    OWNER_USERNAME

    USERNAME
    MESSAGE

    USERNAME
    MESSAGE
    ```
- Direct Messages folder: contains text files for each direct message combination, in format `USERNAME USERNAME.txt`
  - Each file is in the format:
    ```
    NAME

    USERNAME
    MESSAGE

    USERNAME
    MESSAGE
    ```

## User interface

In this part, you're gonna see the general rules for the UI. You must create a system which implements these rules, and then all pages/windows, defined in the following section, must utilize that system (so for each page, there is just a definition of what's inside it).

Note: the `█` character, represents the location of the cursor

- Errors are always surrounded by a double border:
  ```
  ╔════════════════════╗
  ║ An error occurred! ║
  ╚════════════════════╝
  ```
  and must be shown at the top of the page.
- Menu items (navigation bar items) are always surrounded by a single border:
  ```
  ┌────────┬────────┬────────┐
  │ Page 1 │ Page 2 │ Page 3 │
  └────────┴────────┴────────┘
  ```
  - At the left side, there is always a "rail" that connects all *fields* to the menu items. Errors are never connected.
    ```
    ╔══════════════════╗
    ║ Invalid message! ║
    ╚══════════════════╝
    ┌────────┬────────┬────────┐
    │ Page 1 │ Page 2 │ Page 3 │
    ├────────┴────────┴────────┘
    ├% steve : This is a test message
    ├% john : It's true
    ├% steve : And this is another test
    └@ Message : █
    ```
- Text fields just contain some text, and start with a `%`:
  ```
  ├% steve : This is a test message
  ```
  - A notable exception is the page indicator, which is just text, but connects to the "rail", rather than start with a `%`:
    ```
    ├─Page 1/5─┘
    ```
- Input fields take user input and start with a `@`, then have specifications on valid input and finally semicolon and user's cursor:
  ```
  └@ Username : █
  ```
  They're always at the very bottom, unless filled. A filled input field is treated as a text field, but still has the `@` symbol.
  ```
  ├@ Username : John
  └@ Password : █
  ```
  - The specification can be a normal word (like just `Username`, `Password`, etc.) or a command specification. Command specifications are surrounded in square brackets. Capital letters or symbols mean that you can enter that character for the command, while lowercase word(s) specify what type of value is the command. Capital letter commands are case insensitive, so you could just write the lowercase of the letter.
    ```
    └@ [A/B/C/name/number/H] : █
    ```
    *Here, you can either enter the letter A, B, C or H (case insensitive), and that will execute some command. If you enter some sort of a name or number, those will be seen as commands (case sensitive).*
    `H` is the Help command, it shows in a bit more detail the command specifications, it should exist almost everywhere
  - Usually there is a "blank" line above the input field, unless it's directly below the menu items:
    ```
    ┌───────┐
    │ Login │
    ├───────┘
    ├% Logging into ctfc
    │
    └@ Username : █
    ```
    ```
    ┌───────┐
    │ Login │
    ├───────┘
    └@ Username : █
    ```
- Numbered fields are used for navigation purposes. In them, each item starts with with a number and by selecting a number, some action should happen to the selected item:
  ```
  ├1 First Option
  ├2 Second Option
  ├3 Third Option
  ```

### Initial window

When launching the application, the user will be prompted to choose between logging in or registering an account:
```
┌───────┬──────────┐
│ Login │ Register │
├───────┴──────────┘
└@ [L/R] : █
```
If an invalid value is given, the error is shown on top and the prompt is below:
```
╔════════════════════════════════╗
║ "asd" is not a valid argument! ║
╚════════════════════════════════╝
┌───────┬──────────┐
│ Login │ Register │
├───────┴──────────┘
└@ [L/R] : █
```

### Login

In the login screen, you should be greeted with a username prompt:
```
┌───────┐
│ Login │
├───────┘
└@ Username : █
```
After input, if the user does not exist, it will show an error and the prompt below it:
```
╔════════════════════════════╗
║ User "asd" does not exist! ║
╚════════════════════════════╝
┌───────┐
│ Login │
├───────┘
└@ Username : █
```
but if the user exists, it should show the password prompt below:
```
┌───────┐
│ Login │
├───────┘
├@ Username : John
└@ Password : █
```
On incorrect password, there should be an error shown and the prompt below:
```
╔═════════════════════╗
║ Incorrect password! ║
╚═════════════════════╝
┌───────┐
│ Login │
├───────┘
├@ Username : John
└@ Password : █
```
On correct password, user should be greeted by the [chats page](#Chats%20page).

Note: You probably got how it looks when there is an error, so I won't show it anymore.

### Register

The user should be prompted with input prompts in this order (if values is correct):
```
┌──────────┐
│ Register │
├──────────┘
└@ Username [A-z, 0-9, *, ., _, -] : █
```
```
┌──────────┐
│ Register │
├──────────┘
├@ Username : John
└@ Password [5-40 characters; at least 1 number] : █
```
```
┌──────────┐
│ Register │
├──────────┘
├@ Username : John
├@ Password : password1
└@ Name [2-60 characters] : █
```

After pressing enter in the name section, the user should be redirected to the [chats page](#Chats%20page) (so, instantly logged in).

### Chats page

The chats page is one of the main pages inside the application. Here you can see all available chats with pagination, and you can also Create your own chat or Edit ones you own.
```
┌─────────────────┬─────────┬────────┐
│ Direct Messages │ Account │ Logout │
├─────────────────┴─────────┴────────┘
├1 Cool Kids Club : A chat room only for those worthy of it B-)
├2 Computer geeks : You like computers? So do we. Join us for the nerdy experience
├─Page 1/1─┘
│
└@ [D/A/L/</>/C/E/name/number/H] : █
```
With Help message:
```
┌─────────────────┬─────────┬────────┐
│ Direct Messages │ Account │ Logout │
├─────────────────┴─────────┴────────┘
├1 Cool Kids Club : A chat room only for those worthy of it B)
├2 Computer geeks : You like computers? So do we. Join us for the nerdy experience
├─Page 1/1─┘
│
├% Chats page options : [(D)irect messages/(A)ccount/(L)ogout/(<) for previous page/(>) for next page/(C) for create chat/(E) for edit chats/(name) for go to chat room by name/(number) for go to chat room by number/(H)elp]
└@ [D/A/L/</>/C/E/name/number/H] : █
```

### Create chat page

```
┌─────────────────────┐
│ Creating a new chat │
├─────────────────────┘
└@ Name [2-20 characters; A-z, 0-9, spaces, _, -] or [B/H] : █
```
```
┌─────────────────────┐
│ Creating a new chat │
├─────────────────────┘
├% Creating a new chat options : [(B)ack to chats page/(H)elp]
└@ Name [2-20 characters; A-z, 0-9, spaces, _, -] or [B/H] : █
```
```
┌─────────────────────┐
│ Creating a new chat │
├─────────────────────┘
├@ Name : Computer geeks
└@ Description [0-30 characters] : █
```

### Edit chat page

```
┌───────────────┐
│ Edit own chat │
├───────────────┘
├1 Cool Kids Club : A chat room only for those worthy of it B-)
├─Page 1/1─┘
│
└@ [B/</>/name/number/H] : █
```
```
┌───────────────┐
│ Edit own chat │
├───────────────┘
├1 Cool Kids Club : A chat room only for those worthy of it B-)
├─Page 1/1─┘
│
├% Edit own chat options : [(B)ack to chats page/(<) for previous page/(>) for next page/(name) to edit chat room with name/(number) to edit chat room with number/(H)elp]
└@ [B/</>/name/number/H] : █
```

```
┌──────────────┐
│ Editing chat │
├──────────────┘
├1 Chat name : Cool Kids Club
├2 Description : A chat room only for those worthy of it B-)
│
└@ [B/name/number/H] : █
```
```
┌──────────────┐
│ Editing chat │
├──────────────┘
├1 Chat name : Cool Kids Club
├2 Description : A chat room only for those worthy of it B-)
│
├% Editing chat options : [(B)ack to chats page/(number) for edit option by number/(H)elp]
└@ [B/number/H] : █
```

Editing an option is just like editing an [account option](#Account%20page). Also, I'll only show editing Name, since the procedure is the same for editing the Description.

```
┌───────────────────┐
│ Editing chat name │
├───────────────────┘
└@ Account Password or [B/H] : █
```
```
┌───────────────────┐
│ Editing chat name │
├───────────────────┘
├% Editing chat name options : [(B)ack to Editing chat page/(H)elp]
└@ Account Password or [B/H] : █
```
```
┌───────────────────┐
│ Editing chat name │
├───────────────────┘
├@ Account Password : password1
└@ New name [2-20 characters; A-z, 0-9, spaces, _, -] : █
```


### Specific chat page

```
┌───────┬─────────────────┬─────────┬────────┐
│ Chats │ Direct Messages │ Account │ Logout │
├───────┴─────────────────┴─────────┴────────┘
├% Computer geeks : You like computers? So do we. Join us for the nerdy experience
├% Brought to you by "David Robertson (robertson94)"
│
├% robertson94 : I think the new Windows 11 design looks horrible
├% steve : Yeah, right??
├─Page 50/50─┘
│
└@ Message or [C/D/A/L/</>/H] : █
```
With Help message:
```
┌───────┬─────────────────┬─────────┬────────┐
│ Chats │ Direct Messages │ Account │ Logout │
├───────┴─────────────────┴─────────┴────────┘
├% Computer geeks : You like computers? So do we. Join us for the nerdy experience
├% Brought to you by "David Robertson (robertson94)"
│
├% robertson94 : I think the new Windows 11 design looks horrible
├% steve : Yeah, right??
├─Page 50/50─┘
│
├% Chat options : [(C)hats/(D)irect messages/(A)ccount/(L)ogout/(<) for previous page/(>) for next page/(H)elp]
└@ Message or [C/D/A/L/</>/H] : █
```

**Note:** since a user might want to message with a letter/character **that could be interpreted as a command option**, putting a `\` in front must prevent the command from getting executed and just type in the letter:
```
┌───────┬─────────────────┬─────────┬────────┐
│ Chats │ Direct Messages │ Account │ Logout │
├───────┴─────────────────┴─────────┴────────┘
├% Computer geeks : You like computers? So do we. Join us for the nerdy experience
├% Brought to you by "David Robertson (robertson94)"
│
├% robertson94 : I think the new Windows 11 design looks horrible
├% steve : Yeah, right??
├─Page 50/50─┘
│
└@ Message or [C/D/A/L/</>/H] : \L█
```
```
┌───────┬─────────────────┬─────────┬────────┐
│ Chats │ Direct Messages │ Account │ Logout │
├───────┴─────────────────┴─────────┴────────┘
├% Computer geeks : You like computers? So do we. Join us for the nerdy experience
├% Brought to you by "David Robertson (robertson94)"
│
├% robertson94 : I think the new Windows 11 design looks horrible
├% steve : Yeah, right??
├% John : L
├─Page 50/50─┘
│
└@ Message or [C/D/A/L/</>/H] : █
```
- Sending `\\L` should result in `\L` message. It's not a problem if you just remove the first `\` in a message, so if someone sends `\B`, it's fine if it produces `B` as message.
- Letters that can't be interpreted as a command should be able to be written without worry. In the example above, if you enter `B`, it should just result in a message with `B`

### Direct messages page

```
┌───────┬─────────┬────────┐
│ Chats │ Account │ Logout │
├───────┴─────────┴────────┘
├1 David Robertson (robertson94)
├2 Bob J Stevenson (bjs)
├3 Steve (steve)
├─Page 2/2─┘
│
└@ [C/A/L/</>/username/number/H] : █
```
With Help message:
```
┌───────┬─────────┬────────┐
│ Chats │ Account │ Logout │
├───────┴─────────┴────────┘
├1 David Robertson (robertson94)
├2 Bob J Stevenson (bjs)
├3 Steve (steve)
├─Page 2/2─┘
│
├% Direct messages options : [(C)hats/(A)ccount/(L)ogout/(<) for previous page/(>) for next page/(username) for directly message person with username/(number) for enter direct messages by number/(H)elp]
└@ [C/A/L/</>/username/number/H] : █
```

When chatting with a person:
```
┌───────┬─────────────────┬─────────┬────────┐
│ Chats │ Direct Messages │ Account │ Logout │
├───────┴─────────────────┴─────────┴────────┘
├% David Robertson (robertson94)
│
├% robertson94 : Hello
├% John : Hi
├─Page 1/1─┘
│
└@ Message or [C/D/A/L/</>/H] : █
```
With Help message:
```
┌───────┬─────────────────┬─────────┬────────┐
│ Chats │ Direct Messages │ Account │ Logout │
├───────┴─────────────────┴─────────┴────────┘
├% David Robertson (robertson94)
│
├% robertson94 : Hello
├% John : Hi
├─Page 1/1─┘
│
├% Direct chat options : [(C)hats/(D)irect messages/(A)ccount/(L)ogout/(<) for previous page/(>) for next page/(H)elp]
└@ Message or [C/D/A/L/</>/H] : █
```

**Note:** the same rule about writing a message that is just a letter (**that could be interpreted as a command**) in chats applies here. Adding a `\` in front of the letter prevents it from getting executed (and the letter gets sent as text):
```
┌───────┬─────────────────┬─────────┬────────┐
│ Chats │ Direct Messages │ Account │ Logout │
├───────┴─────────────────┴─────────┴────────┘
├% David Robertson (robertson94)
│
├% robertson94 : Hello
├% John : Hi
├─Page 1/1─┘
│
└@ Message or [C/D/A/L/</>/H] : \L█
```
```
┌───────┬─────────────────┬─────────┬────────┐
│ Chats │ Direct Messages │ Account │ Logout │
├───────┴─────────────────┴─────────┴────────┘
├% David Robertson (robertson94)
│
├% robertson94 : Hello
├% John : Hi
├% John : L
├─Page 1/1─┘
│
└@ Message or [C/D/A/L/</>/H] : █
```
- Sending `\\L` should result in `\L` message. It's not a problem if you just remove the first `\` in a message, so if someone sends `\B`, it's fine if it produces `B` as message.
- Letters that can't be interpreted as a command should be able to be written without worry. In the example above, if you enter `B`, it should just result in a message with `B`

### Account page

```
┌───────┬─────────────────┬────────┐
│ Chats │ Direct Messages │ Logout │
├───────┴─────────────────┴────────┘
├% Username : John
├1 Password : [HIDDEN]
├2 Name     : John Smith
│
└@ [C/D/L/number/H] : █
```
With Help message:
```
┌───────┬─────────────────┬────────┐
│ Chats │ Direct Messages │ Logout │
├───────┴─────────────────┴────────┘
├% Username : John
├1 Password : [HIDDEN]
├2 Name     : John Smith
│
├% Account options : [(C)hats/(D)irect Messages/(L)ogout/(number) for edit option by number/(H)elp]
└@ [C/D/L/number/H] : █
```

Editing an option requires you first input your password, then entering a new value for the chosen property (**don't forget to show the specification**). I'll only show an example for updating password:

```
┌──────────────────────────┐
│ Editing Account Password │
├──────────────────────────┘
└@ Password or [B/H] : █
```
```
┌──────────────────────────┐
│ Editing Account Password │
├──────────────────────────┘
├% Editing Account Password options : [(B)ack to account page/(H)elp]
└@ Password or [B/H] : █
```
```
┌──────────────────────────┐
│ Editing Account Password │
├──────────────────────────┘
├@ Password or [B/H] : password1
└@ New Password [5-40 characters; at least 1 number] : █
```

### Logout page

```
┌───────────────────────────────────────────┐
│ Are you sure you want to Log out of John? │
├───────────────────────────────────────────┘
│
└@ [Y/N] : █
```
Any option that isn't "Y" (and "y") should return the user to the chats page.
In place of John, there should be the username of the logged in user.

## Shortcomings

As I mentioned, this is meant to be an exercise task, not a real project. But, if you want to turn it into something useable, here are some suggestions on what you could do:
- Implement deletion of accounts and messages
- Replace text files with an actual database (because the file system doesn't scale at all)
- Add networking and maybe server application (or make it peer-to-peer)
- Implement friends and notifications and viewing accounts inside chats
- Add a biography/description field inside profiles
- Add profile email and implement a mail service (send password confirmation emails and such)
- Implement UI customization, or just rework the UI as a whole

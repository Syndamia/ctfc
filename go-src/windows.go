package ctfc

import (
	"strings"

	"gitlab.com/Syndamia/ctfc/go-src/csi"
	"gitlab.com/Syndamia/ctfc/go-src/ui"
	"gitlab.com/Syndamia/ctfc/go-src/utils"
)

const showHelp = "ShowHelp"

type window func(...string)

func StartupWindow(...string) {
	csi.ClearScreen()

	ui.NormalBox(true, loginNavTitle, registerNavTitle)
	input := ui.InputField(startupWindowSpec)

	nextWindow := handleInputActions(input, false,
		inputAction{"L", loginWindow, nil},
		inputAction{"R", registerWindow, nil},
	)

	if nextWindow == nil {
		defer showError(invalidCommand, StartupWindow)
	} else {
		defer nextWindow()
	}
}

func loginWindow(values ...string) {
	// Takes form user input
	var inputs []string
	formWindow(loginNavTitle, StartupWindow,
		[]formInput{
			{usernameInName, inputBackSpec, nil},
			{passwordInName, "", nil},
		},
		&inputs,
	)

	if !usernameExists(inputs[0]) {
		defer showError(valueDoesNotExist(usernameInName), loginWindow)
	}

	// If login is successful, go to chats window, else show error
	if logInUser(inputs[0], inputs[1]) {
		defer chatsWindow()
	} else {
		defer showError(invalidCredentials, loginWindow, inputs[0])
	}
}

func registerWindow(values ...string) {
	var inputs []string
	formWindow(registerNavTitle, StartupWindow,
		[]formInput{
			{usernameInName, usernameSpec, stringValidUsername},
			{passwordInName, passwordSpec, stringValidPassword},
			{nameInName, nameSpec, stringValidName},
		},
		&inputs,
	)

	if usernameExists(inputs[0]) {
		defer showError(valueAlreadyTaken(usernameInName), registerWindow, values...)
		return
	}

	createUser(inputs)
	logInUser(inputs[0], inputs[1])
	defer chatsWindow()
}

func chatsWindow(values ...string) {
	csi.ClearScreen()

	initPaginatedValues(0, &values)

	ui.NormalBox(true, directMessagesNavTitle, accountNavTitle, logoutNavTitle)

	allChats := getAllChats()
	ui.EmptyLines(pageSize + 3) // empty line + message lines + page number line + empty line

	customLinesBelow := utils.If(values[1] == showHelp).Int(3, 2)

	lastLine := -1
	go routinePaginatedSubwindow(&allChats,
		func(s *[]string) { *s = getAllChats() },
		&lastLine,
		len(values[0]),
		customLinesBelow,
		true,
	)

	if values[1] == showHelp {
		ui.TextField(chatsWindowHelpMsg)
	}

	input := ui.InputField(chatsSpec)

	nextWindow := handleInputActions(input, true,
		inputAction{"C", createChatWindow, nil},
		inputAction{"E", editChatsWindow, nil},
		inputAction{"H", chatsWindow, []string{values[0], showHelp}},
	)

	if nextWindow == nil {
		defer handleComplexInputActions(input,
			allChats,
			" : ",
			&values[0],
			chatsWindow,
			values,
			chatWindow,
			chatNameExists,
		)
	} else {
		defer nextWindow()
	}
	lastLine = -2
}

func createChatWindow(values ...string) {
	var inputs []string
	formWindow(createChatNavTitle, chatsWindow,
		[]formInput{
			{chatNameInName, chatNameSpec, stringValidChatName},
			{chatDescInName, chatDescSpec, stringValidChatDesc},
		},
		&inputs,
	)
	if chatNameExists(inputs[0]) {
		defer showError(valueAlreadyTaken(chatNameInName), createChatWindow, values...)
		return
	}

	createChat(inputs[0], inputs[1], loggedInUser.Username)
	defer chatWindow(inputs[0])
}

func editChatsWindow(values ...string) {
	csi.ClearScreen()

	initPaginatedValues(0, &values)

	ui.NormalBox(true, editChatsNavTitle)

	ownChats := getOwnChats()
	ui.EmptyLines(pageSize + 3) // empty line + message lines + page number line + empty line

	customLinesBelow := utils.If(values[1] == showHelp).Int(3, 2)

	lastLine := -1
	go routinePaginatedSubwindow(&ownChats,
		func(s *[]string) { *s = getOwnChats() },
		&lastLine,
		len(values[0]),
		customLinesBelow,
		true,
	)

	if values[1] == showHelp {
		ui.TextField(editChatsWindowHelpMsg)
	}

	input := ui.InputField(editChatsWindowSpec)

	nextWindow := handleInputActions(input, true,
		inputAction{"B", chatsWindow, nil},
		inputAction{"H", editChatsWindow, []string{values[0], showHelp}},
	)

	if nextWindow == nil {
		defer handleComplexInputActions(input,
			ownChats,
			" : ",
			&values[0],
			editChatsWindow,
			values,
			editChatWindow,
			chatNameExists,
		)
	} else {
		defer nextWindow()
	}
	lastLine = -2
}

func editChatWindow(values ...string) {
	csi.ClearScreen()

	currChat := getChat(values[0])

	ui.NormalBox(true, editChatNavTitle)
	ui.NumberedFields(
		chatNameInName+" : "+currChat.Name,
		chatDescInName+" : "+currChat.Description,
	)

	ui.EmptyLine()
	if len(values) > 1 {
		if values[1] == showHelp {
			ui.TextField(editChatWindowHelpMsg)
		}
	}
	input := ui.InputField(editChatWindowSpec)

	nextWindow := handleInputActions(input, false,
		inputAction{"B", editChatsWindow, nil},
		inputAction{"H", editChatWindow, []string{values[0], showHelp}},
	)

	if nextWindow == nil {
		forReturn := func(...string) {
			defer editChatWindow(values...)
		}
		defer validatedMultiForm(input, forReturn,
			multiFormProp{int(chatNameProp), editChatNameNavTitle,
				formInput{newChatNameInName, chatNameSpec, stringValidChatName},
				func(fvalues []string) bool { return currChat.UpdateName(fvalues[0], fvalues[1]) },
				nil,
			},
			multiFormProp{int(chatDescriptionProp), editNameNavTitle,
				formInput{newChatDescInName, chatDescSpec, stringValidChatDesc},
				func(fvalues []string) bool { return currChat.UpdateDescription(fvalues[0], fvalues[1]) },
				nil,
			},
		)
	} else {
		defer nextWindow()
	}
}

func chatWindow(values ...string) {
	csi.ClearScreen()

	// We determine page number by length of a string
	// This method should be faster than having to cast to int all the time
	initPaginatedValues(1, &values)

	currChat := getChat(values[0])

	ui.NormalBox(true, chatNavTitle, directMessagesNavTitle, accountNavTitle, logoutNavTitle)
	ui.TextFields(
		currChat.Name+" : "+currChat.Description,
		"Brought to you by "+currChat.Owner.Name+" ("+currChat.Owner.Username+")",
	)

	ui.EmptyLines(pageSize + 3) // empty line + messages lines + page number line + empty line

	customLinesBelow := utils.If(values[2] == showHelp).Int(3, 2)

	lastLine := -1
	go routinePaginatedSubwindow(&currChat.Messages,
		func(s *[]string) { *s = getChat(values[0]).Messages },
		&lastLine,
		len(values[1]),
		customLinesBelow,
		false,
	)

	if values[2] == showHelp {
		ui.TextField(chatWindowHelpMsg)
		values[2] = ""
	}

	input := ui.InputField(chatWindowSpec)

	nextWindow := handleInputActions(input, true,
		inputAction{"H", chatWindow, []string{values[0], values[1], showHelp}},
	)

	if nextWindow == nil {
		switch input {
		case ">":
			// If possible, increment to the next page (adds a dot to the end of the string)
			if len(values[1]) < totalPages(len(currChat.Messages)) {
				values[1] += "."
			}
		case "<":
			// If possible, decrement to the previous page (removes a dot from the string)
			if len(values[1]) > 1 {
				utils.StrShortenRight(&values[1], 1)
			}
		default: // Send a message
			values[1] = "." // Make sure to be at the first page, when sending a message
			// Since "C" is a command, to just enter the letter C you'll need to input "\C"
			currChat.addMessage(strings.TrimPrefix(input, "\\"), loggedInUser.Username)
		}

		defer chatWindow(values...)
	} else {
		defer nextWindow()
	}
	lastLine = -2 // Practically stops execution of the paginated subwindow routine
}

func directMessagesWindow(values ...string) {
	csi.ClearScreen()

	initPaginatedValues(0, &values)

	ui.NormalBox(true, chatNavTitle, accountNavTitle, logoutNavTitle)

	allDirectMessageChats := getAllDirectMessageChats()
	ui.EmptyLines(pageSize + 3) // empty line + message lines + page number line + empty line

	customLinesBelow := utils.If(values[1] == showHelp).Int(3, 2)

	lastLine := -1
	go routinePaginatedSubwindow(&allDirectMessageChats,
		func(s *[]string) { *s = (getAllDirectMessageChats()) },
		&lastLine,
		len(values[0]),
		customLinesBelow,
		true,
	)

	if values[1] == showHelp {
		ui.TextField(directMessagesWindowHelpMsg)
	}

	input := ui.InputField(chatsSpec)

	nextWindow := handleInputActions(input, true,
		inputAction{"H", directMessagesWindow, []string{values[0], showHelp}},
	)

	if nextWindow == nil {
		values[0] = values[0][:len(values[0])-1]
		defer handleComplexInputActions(input,
			allDirectMessageChats,
			" (",
			&values[0],
			directMessagesWindow,
			values,
			directMessageWindow,
			usernameExists,
		)
	} else {
		defer nextWindow()
	}
	lastLine = -2
}

func directMessageWindow(values ...string) {
	csi.ClearScreen()

	// We determine page number by length of a string
	// This method should be faster than having to cast to int all the time
	initPaginatedValues(1, &values)

	currDirectMessagesChat := getDirectMessageChat(values[0])

	ui.NormalBox(true, chatNavTitle, directMessagesNavTitle, accountNavTitle, logoutNavTitle)
	ui.TextField(currDirectMessagesChat.Author2.Name + " (" + currDirectMessagesChat.Author2.Username + ")")

	ui.EmptyLines(pageSize + 3) // empty line + messages lines + page number line + empty line

	customLinesBelow := utils.If(values[2] == showHelp).Int(3, 2)

	lastLine := -1
	go routinePaginatedSubwindow(&currDirectMessagesChat.Messages,
		func(s *[]string) { *s = getDirectMessageChat(values[0]).Messages },
		&lastLine,
		len(values[1]),
		customLinesBelow,
		false,
	)

	if values[2] == showHelp {
		ui.TextField(directMessageWindowHelpMsg)
		values[2] = ""
	}

	input := ui.InputField(chatWindowSpec)

	nextWindow := handleInputActions(input, true,
		inputAction{"H", directMessageWindow, []string{values[0], values[1], showHelp}},
	)

	if nextWindow == nil {
		switch input {
		case ">":
			// If possible, increment to the next page (adds a dot to the end of the string)
			if len(values[1]) < totalPages(len(currDirectMessagesChat.Messages)) {
				values[1] += "."
			}
		case "<":
			// If possible, decrement to the previous page (removes a dot from the string)
			if len(values[1]) > 1 {
				utils.StrShortenRight(&values[1], 1)
			}
		default: // Send a message
			values[1] = "." // Make sure to be at the first page, when sending a message
			// Since "C" is a command, to just enter the letter C you'll need to input "\C"
			currDirectMessagesChat.addMessage(strings.TrimPrefix(input, "\\"), loggedInUser.Username)
		}

		defer directMessageWindow(values...)
	} else {
		defer nextWindow()
	}
	lastLine = -2 // Practically stops execution of the paginated subwindow routine
}

func accountWindow(values ...string) {
	csi.ClearScreen()

	ui.NormalBox(true, chatNavTitle, directMessagesNavTitle, logoutNavTitle)
	ui.TextField(usernameInName + " : " + loggedInUser.Username)
	ui.NumberedFields(
		passwordInName+" : "+hiddenValue,
		nameInName+" : "+loggedInUser.Name,
	)

	ui.EmptyLine()
	if len(values) > 0 {
		if values[0] == showHelp {
			ui.TextField(accountWindowHelpMsg)
		}
	}
	input := ui.InputField(accountWindowSpec)

	nextWindow := handleInputActions(input, true,
		inputAction{"H", accountWindow, []string{showHelp}},
	)

	if nextWindow == nil {
		defer validatedMultiForm(input, accountWindow,
			multiFormProp{int(userPasswordProp), editPasswordNavTitle,
				formInput{newPasswordInName, passwordSpec, stringValidPassword},
				func(fvalues []string) bool { return loggedInUser.UpdatePassword(fvalues[0], fvalues[1]) },
				nil,
			},
			multiFormProp{int(userNameProp), editNameNavTitle,
				formInput{newNameInName, nameSpec, stringValidName},
				func(fvalues []string) bool { return loggedInUser.UpdateName(fvalues[0], fvalues[1]) },
				func(fvalues []string) { loggedInUser.Name = fvalues[1] },
			},
		)
	} else {
		defer nextWindow()
	}
}

func logoutWindow(...string) {
	csi.ClearScreen()

	ui.NormalBox(true, "Are you sure you want to Log out of "+loggedInUser.Username+"?")
	ui.EmptyLine()

	input := ui.InputField(logoutWindowSpec)

	if strings.ToLower(input) == "y" {
		logoutUser()
		defer StartupWindow()
	} else {
		defer chatsWindow()
	}
}

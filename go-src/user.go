package ctfc

import (
	"bytes"
	"os"

	"gitlab.com/Syndamia/ctfc/go-src/folderPaths"
)

var loggedInUser User

type User struct {
	Username string
	Name     string
}

type UserProp int

const (
	userPasswordProp UserProp = iota + 1
	userNameProp
)

func (u User) ValidatePassword(password string) bool {
	f, _ := os.ReadFile(folderPaths.FileAtUsersFolder(u.Username))
	passHash := bytes.Split(f, []byte("\n"))[1]
	return string(passHash) == password
}

func (u User) updateProp(currentPassword string, newValue string, userProp UserProp) bool {
	if !u.ValidatePassword(currentPassword) {
		return false
	}
	var fileContents []byte

	switch userProp {
	case userPasswordProp:
		fileContents = []byte(u.Username + "\n" + newValue + "\n" + u.Name)
	case userNameProp:
		fileContents = []byte(u.Username + "\n" + currentPassword + "\n" + newValue)
	default:
		return false
	}

	os.WriteFile(
		folderPaths.FileAtUsersFolder(u.Username),
		fileContents,
		0644)
	return true
}

func (u User) UpdatePassword(oldPassword string, newPassword string) bool {
	return u.updateProp(oldPassword, newPassword, userPasswordProp)
}

func (u User) UpdateName(password string, newName string) bool {
	return u.updateProp(password, newName, userNameProp)
}

func logoutUser() {
	loggedInUser.Username = ""
	loggedInUser.Name = ""
}

func createUser(data []string) {
	f, _ := os.Create(folderPaths.FileAtUsersFolder(data[0]))
	f.WriteString(data[0] + "\n" + data[1] + "\n" + data[2])
	f.Close()
}

func logInUser(username string, password string) bool {
	if cu := getUser(username); cu.ValidatePassword(password) {
		loggedInUser = cu
		return true
	}
	return false
}

func getUser(username string) User {
	f, _ := os.ReadFile(folderPaths.FileAtUsersFolder(username))
	values := bytes.Split(f, []byte("\n"))
	return User{string(values[0]), string(values[2])}
}

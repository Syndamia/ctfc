package csi

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
)

const (
	ansiCUUf = "\033[%vA" // Cursor (move) Up, formatted version (expects to be given a number)
	ansiCUDf = "\033[%vB" // Cursor (move) Down, formated version (expects to be given a number)
	ansiCUFf = "\033[%vC" // Cursor (move) Forward (to the right), formatted version (expects to be given a number)

	ansiCUU = "\033[A"  // Cursor (move) Up
	ansiCUD = "\033[B"  // Cursor (move) Down
	ansiCUF = "\033[C"  // Cursor (move) Forward (to the right)
	ansiCNL = "\033[E"  // Cursor (move) Next Line
	ansiED  = "\033[2J" // Erase in Display (clears entire screen)
	ansiEL  = "\033[K"  // Erase in Line (clears from cursor to end of line)

	ansiSCP = "\033[s" // Save Current Cursor Position
	ansiRCP = "\033[u" // Restore Saved Cursor Position
)

func MoveCursorUpN(times int) {
	fmt.Printf(ansiCUUf, times)
}

func MoveCursorDownN(times int) {
	fmt.Printf(ansiCUDf, times)
}

func MoveCursorRightN(times int) {
	fmt.Printf(ansiCUFf, times)
}

func MoveCursorUp() {
	fmt.Print(ansiCUU)
}

func MoveCursorDown() {
	fmt.Print(ansiCUD)
}

func MoveCursorRight() {
	fmt.Printf(ansiCUF)
}

func MoveCursorToBeginningOfLine() {
	fmt.Print(ansiCNL)
}

func ClearLine() {
	fmt.Print(ansiEL)
}

var blockClearing = false

func ClearScreen() {
	if blockClearing {
		blockClearing = false
		return
	}

	if runtime.GOOS == "windows" {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	} else {
		fmt.Println(ansiED)
	}
}

// The next time that ClearScreen is called, it won't clear the screen
func BlockClearScreenNextTime() {
	blockClearing = true
}

func EraseToEndOfLine() {
	fmt.Print(ansiEL)
}

func SaveCursorPosition() {
	fmt.Print(ansiSCP)
}

func RestoreCursorPosition() {
	fmt.Print(ansiRCP)
}

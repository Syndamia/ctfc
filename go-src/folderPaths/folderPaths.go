package folderPaths

import (
	"os"
	"runtime"

	"gitlab.com/Syndamia/ctfc/go-src/utils"
)

func InitFoldersAndFiles() {
	dirs := []string{rootFolder(), FileAtChatsFolder(""), FileAtDirectMessagesFolder(""), FileAtUsersFolder("")}
	for _, v := range dirs {
		if !utils.PathExists(v) {
			utils.CreateDir(v)
		}
	}

	files := []string{AllChatsFilePath()}
	for _, v := range files {
		if !utils.PathExists(v) {
			utils.CreateFile(v)
		}
	}
}

func AllChatsFilePath() string {
	return buildPath(rootFolder(), "AllChats.txt")
}

func FileAtChatsFolder(fileName string) string {
	if fileName != "" {
		fileName += ".txt"
	}
	return buildPath(rootFolder(), "Chats", fileName)
}

func FileAtDirectMessagesFolder(fileName string) string {
	if fileName != "" {
		fileName += ".txt"
	}
	return buildPath(rootFolder(), "DirectMessages", fileName)
}

func FileAtUsersFolder(fileName string) string {
	if fileName != "" {
		fileName += ".txt"
	}
	return buildPath(rootFolder(), "Users", fileName)
}

func rootFolder() string {
	if runtime.GOOS == "windows" {
		return "C:\\Users\\username\\AppData\\Roaming\\ctfc"
	}
	home, _ := os.UserHomeDir()
	return home + "/Desktop/ctfc"
}

func buildPath(folders ...string) (path string) {
	sep := "/"
	if runtime.GOOS == "windows" {
		sep = "\\"
	}
	for i, v := range folders {
		path += v
		if i < len(folders)-1 {
			path += sep
		}
	}
	return
}

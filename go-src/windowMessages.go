package ctfc

const (
	// Navgiation
	loginNavTitle          = "Login"
	registerNavTitle       = "Register"
	directMessagesNavTitle = "Direct Messages"
	accountNavTitle        = "Account"
	logoutNavTitle         = "Logout"
	createChatNavTitle     = "Creating a new chat"
	editChatsNavTitle      = "Edit own chat"
	editChatNavTitle       = "Editing chat"
	editChatNameNavTitle   = "Editing chat name"
	editChatDescNavTitle   = "Editing chat description"
	chatNavTitle           = "Chats"
	editPasswordNavTitle   = "Editing Password"
	editNameNavTitle       = "Editing Name"

	// Help messages
	chatsWindowHelpMsg          = "Chats page options: [(D)irect messages/(A)ccount/(L)ogout/(<) for previous page/(>) for next page/(C) for create chat/(E) for edit chats/(name) for go to chat room by name/(number) for go to chat room by number/(H)elp]"
	chatWindowHelpMsg           = "Chat options : [(C)hats/(D)irect messages/(A)ccount/(L)ogout/(<) for previous page/(>) for next page/(H)elp]"
	editChatsWindowHelpMsg      = "Edit own chat options : [(B)ack to chats page/(<) for previous page/(>) for next page/(name) to edit chat room with name/(number) to edit chat room with number/(H)elp]"
	editChatWindowHelpMsg       = "Editing chat options : [(B)ack to chats page/(number) for edit option by number/(H)elp]"
	directMessagesWindowHelpMsg = "Direct Messages page options: [(C)hats/(A)ccount/(L)ogout/(<) for previous page/(>) for next page/(name) for start chatting with username/(number) for go to direct messages by number/(H)elp]"
	directMessageWindowHelpMsg  = "Direct Message options : [(C)hats/(D)irect messages/(A)ccount/(L)ogout/(<) for previous page/(>) for next page/(H)elp]"
	accountWindowHelpMsg        = "Account options : [(C)hats/(D)irect Messages/(L)ogout/(number) for edit option by number/(H)elp]"

	// Input names
	usernameInName        = "Username"
	passwordInName        = "Password"
	nameInName            = "Name"
	chatNameInName        = "Chat name"
	chatDescInName        = "Description"
	newChatNameInName     = "New chat name"
	newChatDescInName     = "New chat description"
	currentPasswordInName = "Current password"
	newPasswordInName     = "New password"
	newNameInName         = "New name"

	// Input specifications
	startupWindowSpec   = "[L/R]"
	chatWindowSpec      = "Message or [C/D/A/L/</>/H]"
	editChatsWindowSpec = "[B/</>/name/number/H]"
	editChatWindowSpec  = "[B/number/H]"
	accountWindowSpec   = "[C/D/L/number/H]"
	logoutWindowSpec    = "[Y/N]"
	inputBackSpec       = " or [B for Back to start page]"
	usernameSpec        = " [A-z, 0-9, *, ., _, -; at least 1 letter] or [B for Back to start page]"
	passwordSpec        = " [5-40 characters; at least 1 number]"
	nameSpec            = " [2-60 characters]"
	chatsSpec           = "[D/A/L/</>/C/E/name/number/H]"
	chatNameSpec        = " [2-20 characters; A-z, 0-9, spaces, _, -] or [B for Back to chats page]"
	chatDescSpec        = " [0-30 characters]"

	// Other
	hiddenValue = "[HIDDEN]"
)

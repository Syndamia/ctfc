package main

import (
	ctfc "gitlab.com/Syndamia/ctfc/go-src"
	"gitlab.com/Syndamia/ctfc/go-src/folderPaths"
)

func main() {
	folderPaths.InitFoldersAndFiles()
	ctfc.StartupWindow()
}

# src-go

This is the [`Go`](https://go.dev/) implementation of the exercise.

**This is my first Go project!** As such, a lot is implemented in a stupid way and a lot of corners have been cut.

It took ~35 hours of coding time to implement (tracked with [wakapi](https://github.com/muety/wakapi), which means this time doesn't entierly account for off time, when I've been looking stuff up and not coding).

## Running if repository cloned

```bash
$ go run main/main.go
```

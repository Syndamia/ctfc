package ctfc

import (
	"bytes"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/Syndamia/ctfc/go-src/folderPaths"
	"gitlab.com/Syndamia/ctfc/go-src/utils"
)

type DirectMessageChat struct {
	Author1  User
	Author2  User
	Messages []string
}

func (dmc DirectMessageChat) genFileName() string {
	if dmc.Author1.Username > dmc.Author2.Username {
		return dmc.Author1.Username + " " + dmc.Author2.Username
	}
	return dmc.Author2.Username + " " + dmc.Author1.Username
}

func (dmc DirectMessageChat) addMessage(value string, username string) {
	if dmc.Author1.Username != username && dmc.Author2.Username != username {
		return
	}

	value = username + " : " + value
	dmc.Messages = append(dmc.Messages, value)

	fileName := folderPaths.FileAtDirectMessagesFolder(dmc.genFileName())

	if !utils.PathExists(fileName) {
		utils.CreateFile(fileName)
	}
	utils.AppendToFile(fileName, value+"\n")
}

func getDirectMessageChat(otherAuthorUsername string) (dmc DirectMessageChat) {
	dmc = DirectMessageChat{loggedInUser, getUser(otherAuthorUsername), []string{}}

	if utils.PathExists(folderPaths.FileAtDirectMessagesFolder(dmc.genFileName())) {
		f, _ := os.ReadFile(folderPaths.FileAtDirectMessagesFolder(dmc.genFileName()))
		values := bytes.Split(f, []byte("\n"))
		dmc.Messages = utils.TwoDByteArrayToStringArray(values[:len(values)-1])
	}

	return
}

func getAllDirectMessageChats() (chats []string) {
	filepath.Walk(folderPaths.FileAtDirectMessagesFolder(""), func(path string, info os.FileInfo, err error) error {
		if strings.Contains(path, loggedInUser.Username) {
			authors := utils.StrMSplit(path, "/", ".", " ")
			authors = authors[len(authors)-3 : len(authors)-1]
			otherAuthor := getUser(utils.If(authors[0] == loggedInUser.Username).String(authors[1], authors[0]))
			chats = append(chats, otherAuthor.Name+" ("+otherAuthor.Username+")")
		}
		return nil
	})
	return
}

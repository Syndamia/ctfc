package utils

import (
	"math"
	"os"
	"strings"
)

// Repeats a rune given amount of times and returns the result as a string
func RepeatRune(r rune, times int) (result string) {
	for i := 0; i < times; i++ {
		result += string(r)
	}
	return
}

// Replaces a character inside a string with a given rune at index
//
// Thanks https://stackoverflow.com/a/24894202/12036073
func ReplaceAtIndex(in string, r rune, i int) string {
	out := []rune(in)
	out[i] = r
	return string(out)
}

func TwoDByteArrayToStringArray(in [][]byte) (result []string) {
	for _, v := range in {
		result = append(result, string(v))
	}
	return
}

func AppendToFile(path string, value string) {
	allChatsFile, _ := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, 0644)
	allChatsFile.WriteString(value)
	allChatsFile.Close()
}

func CreateDir(path string) {
	os.Mkdir(path, 0775)
}

func CreateFile(path string) {
	f, _ := os.Create(path)
	f.Close()
}

func PathExists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func GetFileContents(path string) []string {
	input, _ := os.ReadFile(path)
	return strings.Split(string(input), "\n")
}

func StrShortenRight(s *string, amount int) {
	*s = (*s)[:len(*s)-amount]
}

func StrMSplit(s string, seps ...string) (sret []string) {
	for _, sep := range seps {
		if len(sret) == 0 {
			sret = strings.Split(s, sep)
		} else {
			var tmp []string
			for _, sepStr := range sret {
				tmp = append(tmp, strings.Split(sepStr, sep)...)
			}
			sret = tmp
		}
	}
	return
}

func MaxInt(x int, y int) int {
	if x > y {
		return x
	}
	return y
}

func CeilDivInt(x int, y int) int {
	return int(math.Ceil(float64(x) / float64(y)))
}

// Special thanks to icza, over on https://stackoverflow.com/a/59375088 for the If constuction

type If bool

func (c If) Int(a, b int) int {
	if c {
		return a
	}
	return b
}

func (c If) String(a, b string) string {
	if c {
		return a
	}
	return b
}

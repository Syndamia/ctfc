package ctfc

const (
	invalidArgument    = "Invalid argument!"
	invalidCommand     = "Invalid command!"
	invalidCredentials = "Invalid credentials!"
)

func invalidValueFor(typeName string) string {
	return "Invalid value for " + typeName + "!"
}

func valueDoesNotExist(typeName string) string {
	return typeName + " does not exist!"
}

func valueAlreadyTaken(typeName string) string {
	return typeName + " is already taken!"
}

package ctfc

import (
	"os"
	"regexp"

	"gitlab.com/Syndamia/ctfc/go-src/folderPaths"
)

var SpecialCharacters = []rune{'!', '@', '#', '$', '%', '^', '&', '*'}

// Inclusive minimum and maximum
func stringValidLength(min int, max int, value string) bool {
	return len(value) >= min && len(value) <= max
}

func stringValidCharacters(value string, letters bool, numbers bool, special ...rune) bool {
	expr := "^["
	if letters {
		expr += "A-z"
	}
	if numbers {
		expr += "0-9"
	}
	for _, v := range special {
		expr += string(v)
	}
	expr += "]+$"

	return regexp.MustCompile(expr).MatchString(value)
}

func stringValidRuneAmount(value string, runeStart rune, runeEnd rune, minAmount int) bool {
	var sum int
	for _, v := range value {
		if v >= runeStart && v <= runeEnd {
			sum++
		}
	}
	return sum >= minAmount
}

func atLeastOneLetter(value string) bool {
	return stringValidRuneAmount(value, 'A', 'Z', 1) || stringValidRuneAmount(value, 'a', 'z', 1)
}

// Between 2 and 40 characters (A-z, 0-9, ., *, _, -, at least one letter)
func stringValidUsername(value string) bool {
	return stringValidLength(2, 40, value) &&
		stringValidCharacters(value, true, true, '.', '*', '_', '-') &&
		atLeastOneLetter(value)
}

// Between 5 and 40 characters (any character, but at least one number)
func stringValidPassword(value string) bool {
	return stringValidLength(5, 40, value) &&
		stringValidCharacters(value, true, true, SpecialCharacters...) &&
		stringValidRuneAmount(value, '0', '9', 1)
}

// Between 2 and 60 characters (A-z, spaces, ', -)
func stringValidName(value string) bool {
	return stringValidLength(2, 60, value) &&
		stringValidCharacters(value, true, false, ' ', '\'', '-')
}

// Between 2 and 20 characters (A-z, 0-9, spaces, \_, -, at least one letter)
func stringValidChatName(value string) bool {
	return stringValidLength(2, 20, value) &&
		stringValidCharacters(value, true, true, ' ', '_', '-') &&
		atLeastOneLetter(value)
}

// Between 0 and 30 characters (any character)
func stringValidChatDesc(value string) bool {
	return stringValidLength(0, 30, value)
}

func usernameExists(username string) bool {
	_, err := os.Stat(folderPaths.FileAtUsersFolder(username))
	return err == nil
}

func chatNameExists(name string) bool {
	_, err := os.Stat(folderPaths.FileAtChatsFolder(name))
	return err == nil
}
